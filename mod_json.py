#!/usr/bin/env python3
import json
import argparse
import os.path

def do_args():
    parser=argparse.ArgumentParser(description='mirror a loop layout json file')
    parser.add_argument('geometry_file', help='path to geometry file to transform')
    parser.add_argument('operation',help='operation to preform')
    parser.add_argument('--width',type=float,help='canvas width')
    parser.add_argument('--height',type=float,help='canvas height')
    parser.add_argument('--vdist',type=float,default=0,help='vertical distance to move if operation is translate')
    parser.add_argument('--hdist',type=float,default=0,help='horizontal distance to move if operation is translate')
    parser.add_argument('--vscale',type=float,default=1,help='vertical stretching factor to use if operation is scale')
    parser.add_argument('--hscale',type=float,default=1,help='horizontal stretching factor to use if operation is scale')

    return parser.parse_args()

def main():
    args = do_args()
    
    #read input file
    with open(args.geometry_file,"r") as f:
        data =  json.load(f)
    
    #do the modification
    for element_idx in range(len(data)):
        if args.operation == 'mirror_rl':
            data[element_idx]['center'][0] = args.width - data[element_idx]['center'][0]
        if args.operation == 'mirror_ud':
            data[element_idx]['center'][1] = args.height - data[element_idx]['center'][1]
        if args.operation == 'translate':
            data[element_idx]['center'][0] = data[element_idx]['center'][0] + args.hdist
            data[element_idx]['center'][1] = data[element_idx]['center'][1] + args.vdist
        if args.operation == 'scale':
            data[element_idx]['center'][0] = (data[element_idx]['center'][0]-args.width/2)*args.hscale+args.width/2
            data[element_idx]['center'][1] = (data[element_idx]['center'][1]-args.height/2)*args.vscale+args.height/2
            

    #compute output filename
    (basename,ext) = os.path.splitext(args.geometry_file)
    outfile = basename + '_mod' + ext

    #write the output file
    with open(outfile,'w') as f:
        json.dump(data,f,indent=2)

if __name__ == "__main__":
    main()
