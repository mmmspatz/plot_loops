#!/usr/bin/env python3
import json
import argparse
import cairo
import matplotlib.pyplot as plt
import scipy.io

def read_json(filename):
    with open(filename,"r") as f:
        return json.load(f)    
def calc_bbox(geometry,resx,resy):
    left = round(resx*(geometry.center[1] - dims[1]/2))
    right = round(resx*(geometry.center[1] + dims[1]/2))
    top = round(resy*(geometry.center[2] - dims[2]/2))
    bottom = round(resy*(geometry.center[2] + dims[2]/2))

    return [left, bottom, right, top]

def draw_ellipse(cr, x, y, width, height, linewidth, color=(0,0,0), angle=0):
    """
    x      - center x
    y      - center y
    width  - width of ellipse  (in x direction when angle=0)
    height - height of ellipse (in y direction when angle=0)
    angle  - angle in radians to rotate, clockwise
    """
    cr.set_source_rgb(color[0],color[1],color[2])

    cr.save()
    cr.translate(x*72, y*72)
    cr.rotate(angle)
    cr.scale(width*72 / 2.0, height*72 / 2.0)
    cr.new_path()
    cr.arc(0.0, 0.0, 1.0, 0.0, 2.0 * 3.14159)
    cr.close_path()
    cr.restore()
    cr.set_line_width(linewidth*72)
    cr.stroke()

def draw_filled_ellipse(cr, x, y, width, height, color=(0,0,0), angle=0):
    """
    x      - center x
    y      - center y
    width  - width of ellipse  (in x direction when angle=0)
    height - height of ellipse (in y direction when angle=0)
    angle  - angle in radians to rotate, clockwise
    """

    cr.save()
    cr.set_source_rgb(color[0],color[1],color[2])
    cr.translate(x*72, y*72)
    cr.rotate(angle)
    cr.scale(width*72 / 2.0, height*72 / 2.0)
    cr.new_path()
    cr.arc(0.0, 0.0, 1.0, 0.0, 2.0 * 3.14159)
    cr.close_path()
    cr.fill()
    cr.restore()

def draw_centered_text(cr, x, y, text, fontsize=20, color=(0,0,0)):
    cr.save()
    cr.set_source_rgb(color[0],color[1],color[2])
    cr.select_font_face("Courier", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_BOLD)
    cr.set_font_size(fontsize)
    (x_bearing, y_bearing, width, height, dx, dy) = cr.text_extents(text)
    cr.move_to(x*72 - width/2, y*72 + height/2)
    cr.show_text(text)
    cr.restore()

    

def do_args():
    parser = argparse.ArgumentParser(description='Draw coil geometry from json file')
    #parser.add_argument('geometry_file', help='path to geometry file')
    parser.add_argument('-l','--levels_file', help='path to loop level file')
    parser.add_argument('-p','--precision',type=int,default=0, help='number of digits after the decimal point to print')
    parser.add_argument('-m','--maxcol',type=float,default=0, help='maximum value of the colormap')

    return parser.parse_args()

def print_loop(cr,loop,leveldict,precision,maxcolor):
    #colormap 0.0-1.0 to tuple of rgb values
    jet = plt.get_cmap('jet')

    #draw the black outline
    draw_ellipse(cr, loop['center'][0], loop['center'][1], loop['dims'][0], loop['dims'][1], .125)

    if loop['id'] in leveldict:
        level = leveldict[loop['id']]/maxcolor
        #draw the color splash
        draw_filled_ellipse(cr, loop['center'][0], loop['center'][1], loop['dims'][0]*0.4, loop['dims'][1]*0.4, jet(level))
        #draw the level as text
        val = round(leveldict[loop['id']],precision)
        draw_centered_text(cr, loop['center'][0], loop['center'][1], '%.*f'%(precision,val),12)
        #draw_centered_text(cr, loop['center'][0], loop['center'][1], loop['id'],12)


def main():
    args = do_args()    
    layout = []
    layout = read_json('ABD_loops.json')
    layout = layout + read_json('BCK_loops.json')
    layout = layout + read_json('WNG_R_loops.json')
    layout = layout + read_json('WNG_L_loops.json')

    if args.levels_file:
        matlab_data = scipy.io.loadmat(args.levels_file)
        coil_names = matlab_data['coil_names']
        coil_levels = matlab_data['coil_ranks'][0]
        numel = len(matlab_data['coil_names'])
        if args.maxcol:
            maxcol = args.maxcol
        else:
            maxcol = max(coil_levels)
        leveldict = dict(zip(coil_names,coil_levels))

    ABD_file = "ABD_SNR.svg"
    BCK_file = "BCK_SNR.svg"
    WNG_R_file = "WNGR_SNR.svg"
    WNG_L_file = "WNGL_SNR.svg"

    ABD_srf = cairo.SVGSurface(ABD_file,8.5*72,11*72)
    ABD_ctx = cairo.Context(ABD_srf)
    BCK_srf = cairo.SVGSurface(BCK_file,8.5*72,11*72)
    BCK_ctx = cairo.Context(BCK_srf)
    WNG_R_srf = cairo.SVGSurface(WNG_R_file,8.5*72,11*72)
    WNG_R_ctx = cairo.Context(WNG_R_srf)
    WNG_L_srf = cairo.SVGSurface(WNG_L_file,8.5*72,11*72)
    WNG_L_ctx = cairo.Context(WNG_L_srf)

    for loop in layout:
        if loop['id'].startswith('ABD'):
            print_loop(ABD_ctx,loop,leveldict,args.precision,maxcol)
        elif loop['id'].startswith('BCK'):
            print_loop(BCK_ctx,loop,leveldict,args.precision,maxcol)
        elif loop['id'].startswith('WNG_R'):
            print_loop(WNG_R_ctx,loop,leveldict,args.precision,maxcol)
        elif loop['id'].startswith('WNG_L'):
            print_loop(WNG_L_ctx,loop,leveldict,args.precision,maxcol)

    ABD_ctx.show_page()
    ABD_srf.finish()
    BCK_ctx.show_page()
    BCK_srf.finish()
    WNG_R_ctx.show_page()
    WNG_R_srf.finish()
            



if __name__ == "__main__":
    main()
